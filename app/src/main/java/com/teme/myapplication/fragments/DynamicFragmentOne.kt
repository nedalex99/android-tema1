package com.teme.myapplication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.teme.myapplication.R
import kotlinx.android.synthetic.main.fragment_dynamic_one.*

/**
 * A simple [Fragment] subclass.
 */
class DynamicFragmentOne : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dynamic_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn1.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            transaction?.add(R.id.container, DynamicFragmentTwo(), "FragmentTwo")        //Am folosit functia add pentru ca in F2A2 sa pot sterge fragmentul cu remove
            transaction?.replace(R.id.container, DynamicFragmentTwo(), "FragmentTwo")    //Am folosit replace ca fragmentul adaugat sa se suprapuna peste fragmentul vechi
            transaction?.commit()
        }

    }
}
