package com.teme.myapplication.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.teme.myapplication.R
import com.teme.myapplication.activities.SecondActivity
import kotlinx.android.synthetic.main.activity_second.*
import kotlinx.android.synthetic.main.fragment_dynamic_two.*
import kotlinx.android.synthetic.main.fragment_dynamic_two.btn1

/**
 * A simple [Fragment] subclass.
 */
class DynamicFragmentTwo : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dynamic_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val transaction = fragmentManager?.beginTransaction()

        btn1.setOnClickListener {
            //val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.container, DynamicFragmentThree(), "FragmentThree")
            transaction?.addToBackStack("FragmentTwo")
            transaction?.commit()
        }

        btn2.setOnClickListener {
            transaction?.remove(DynamicFragmentOne())
            transaction?.commit()
        }

        btn3.setOnClickListener {
            activity?.finish()
        }
    }

}
