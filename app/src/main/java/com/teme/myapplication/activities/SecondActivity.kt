package com.teme.myapplication.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.teme.myapplication.R
import com.teme.myapplication.fragments.DynamicFragmentOne
import com.teme.myapplication.fragments.DynamicFragmentTwo
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.container, DynamicFragmentOne(), "FragmentOne")
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }
}
